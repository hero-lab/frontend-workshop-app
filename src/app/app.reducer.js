import { combineReducers } from 'redux';

const appReducer = combineReducers({
  /* ADD REDUCERS HERE */
});

export default (state, action) => appReducer(state, action);
