# A2B

This project is based on create-react-app project. More information about that project you can find using below links:

* [https://github.com/facebookincubator/create-react-app](https://github.com/facebookincubator/create-react-app)

## Getting started

### Installation:

Install dependencies using package manager

```sh
$ yarn
```

### Running project

```sh
yarn start
```

Runs the app in development mode.
It opens [http://localhost:3000](http://localhost:3000) automatically

The page will reload if you make edits.
You will see the build errors and lint warnings in the console.

### Testing

```sh
yarn test
```

Runs the test watcher in an interactive mode.
By default, runs tests related to files changes since the last commit.

### Building project

```sh
yarn run build
```

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!
